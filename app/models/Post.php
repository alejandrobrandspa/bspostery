<?php

class Post extends \Eloquent {
	protected $fillable = ['message', 'link', 'image_url'];
}