<?php

class Page extends \Eloquent {
	protected $fillable = ['name', 'page_token', 'page_id'];

}