<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>BS Postery</title>
	<link rel="stylesheet" href="[[ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') ]]">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="[[ asset('css/bspostery.css') ]]">
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">BS Postery</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
 
      </ul>
   

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
	<div class="container">
		@yield('content')
	</div>
	<script src="[[ asset('bower_components/jquery/dist/jquery.min.js') ]]"></script>	
	<script src="[[ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') ]]"></script>
	<script src="[[ asset('plugins/bootstrap-datepicker.js') ]]"></script>
	<script src="[[ asset('plugins/bootstrap-timepicker.min.js') ]]"></script>
	<script src="[[ asset('app/main.js') ]]"></script>
</body>
</html>