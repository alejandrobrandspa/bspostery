@extends('layouts.main')
@section('content')
<div class="row">	
	<div class="col-lg-4 publisher-container">
		<h3>Publicar</h3>
		<hr>
		<form method="POST" action="/schedule" class="form">
			<div class="form-group">
				<textarea name="message" rows="5" placeholder="Mensaje" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<input type="text" name="link" placeholder="link / video / imagen" class="form-control">
			</div>
				<div class="form-group">
				<input type="text" name="url" placeholder="Imagen url" class="form-control">
			</div>
			<div class="row">
			<div class="form-group col-lg-6">
				<input type="text" name="date" placeholder="Fecha" class="form-control datepicker">
			</div>
			<div class="form-group col-lg-6">
				<input type="text" name="time" placeholder="Hora" class="form-control timepicker">
			</div>
			</div>
			<div class="form-group">
				<select class="form-control">
					<option value="173528036161019">Soul Rise</option>
					<option value="1438168499754615">BS Dev</option>
				</select>
			</div>
			<button class="btn">Guardar y publicar</button>

			<button type="submit" class="btn">Publicar ahora</button>
		</form>
	</div>
	
</div>

@stop