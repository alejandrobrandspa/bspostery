<?php 

use bspostery\repositories\UserRepository;

class UserController extends BaseController {

	protected $user;
	
	public function __construct(UserRepository $user)
	{
		$this->user = $user;
	}

	public function register()
	{
		$this->user->store();
	}
}