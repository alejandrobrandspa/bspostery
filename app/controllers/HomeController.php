<?php

use bspostery\publish\Publisher;
use Carbon\Carbon;

class HomeController extends BaseController {
	
	protected $publisher;

	public function __construct()
	{
		$page = Page::find(1);
		$page_token = $page['page_token'];
		$page_id = $page['page_id'];

		$this->publisher = new Publisher($page_token, $page_id);
	}

	public function post()
	{
		$m = str_random(10).' '.Carbon::now();

		$message = $this->publisher->message($m);

		return $this->publisher->post($message);
	}

	public function scheduled()
	{
		$now = Carbon::now('America/Bogota');
		//return $now;
		$posts = Post::where('scheduled', '<', $now)->where('posted', 0)->get();
		
		foreach($posts as $post) {
			$m = Carbon::now();
			$mk = $this->publisher->message($post['message']);
			$publish = $this->publisher->post($mk);

			if ($publish) {
				$p = Post::find($post['id']);
				$p->posted = true;
				$p->save();
			}
		}
		return 'ok';
	}

	public function schedule()
	{
		$message = Input::get('message');
		$schedule = Input::get('date').' '.Input::get('time').':00';
		
		$post = new Post;
		$post->message = $message;
		$post->scheduled = $schedule;
		$post->posted = false;
		$post->save();
		return $post;
	}



	public function PublishNow()
	{
		$post = new Post;
		$post->message = 'test api date'. Carbon::now();
		$post->scheduled = Carbon::now('America/Bogota')->addMinutes(15);
		$post->posted = false;
		$post->save();
	}

	public function feed()
	{
		$posts = Post::orderBy('scheduled', 'desc')->get();
		return View::make('feed.index', compact('posts'));
	}

}