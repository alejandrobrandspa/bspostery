<?php
use bspostery\repositories\PageRepository;

class PageController extends BaseController {

	protected $page;
	
	public function __construct(PageRepository $page)
	{
		$this->page = $page;
	}

	public function storePages()
	{
		$accounts = Facebook::api('/me/accounts');
		return $this->page->storeAccounts($accounts['data']);
	}

}