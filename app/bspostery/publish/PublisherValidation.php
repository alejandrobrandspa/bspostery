<?php namespace bspostery/publish;

class publisherValidation {

	protected $types = ['message', 'link', 'image'];

	public function check($data)
	{
		if($data['message'] && empty($data['image_url']) && empty($data['link'])) {
			return 'message';
		} elseif($data['image_url']) {
			return 'image';
		} else {
			return 'link';
		}
	}
}