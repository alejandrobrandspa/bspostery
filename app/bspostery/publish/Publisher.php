<?php namespace bspostery\publish;

use Facebook;

class Publisher {

	protected $token;
	protected $page_id;
	protected $validation;

 	public function __construct($page_token, $page_id)
 	{
 		$this->page_token = $page_token;
 		$this->page_id = $page_id;
 	}

	public function message($message) 
	{
		$data = [
			'message' => $message
		];

		return $this->addToken($data);
	}

	public function link($message, $link) 
	{
		$data = [
			'message' => $message,
			'link' => $link
		];
		return $this->addToken($data);
	}
	
	public function photo($image_url, $message) 
	{
		$data = [
			'url' => $image_url,
			'message' => $message
		];

		return $this->addToken($data);
	}

	public function addToken($data)
	{
		$token = [
			'access_token' => $this->page_token
		];

		return array_merge($token, $data);
	}

	public function post($data)
	{
		return Facebook::api('/'.$this->page_id.'/feed', 'POST', $data);
	}
}