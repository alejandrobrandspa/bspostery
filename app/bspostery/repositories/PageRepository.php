<?php namespace bspostery\repositories;

class PageRepository {

	public function storeAccounts($accounts)
	{
	
		foreach($accounts as $page)
		{
			$data = [
				'name' => $page['name'],
				'page_token' => $page['access_token'],
				'page_id' => $page['id']
			];

			$page_db = Page::where('page_id', $page['id'])->count();

			if ($page_db > 0) {
				DB::table('pages')->where('page_id', $page['id'])->update($data);
			} else {
				Page::create($data);
			}
		}
		
		return Page::all();
	}

}