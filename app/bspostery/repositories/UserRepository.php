<?php namespace bspostery\repositories;

use Hash;
use Facebook;
use User;
use DB;

class UserRepository {

	public function getUser()
	{
		$token = Facebook::getAccessToken();
		$me = Facebook::api('/me');
		return $user = [
			'email' => $me['email'],
			'password' => Hash::make($me['id']),
			'username' => $me['username'],
			'user_id' => $me['id'],
			'token' => $token
		];
	}

	public function store()
	{	
		$user = $this->getUser();

		$check = $this->check($user);

	
			return User::create($user);
	
	}

	public function check($user)
	{
		return User::where('email', $user['email'])->count();
	}

	public function update($user)
	{
		return DB::table('users')->where('email', $user['email'])->update($user);
	}


}