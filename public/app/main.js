
$(function(){
	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd'
	});

	$('.timepicker').timepicker({
		showMeridian: false,
		minuteStep: 15
	});
})
